<section class="footer padding-tb-40">
		<div class="container footer-container">
			<div class="row">
				<div class="col-sm-12 col-md-12 padding-tb-40">
					<div class="col-sm-12 col-md-3 textcenter wow fadeInDown">
						<h2><strong>UniqueTheme</strong></h2>
						<p>Contact us for any support</p>
						<h4>Call 111 2222 3333</h4>
						<span>or</span>
						<span class=""></span>
					</div>
					<div class="col-sm-4 col-md-2 padding-tb-15 wow fadeInDown" data-wow-delay="0.1s">
						<h5 class="textcenter footer-heads"><strong>CUSTOMER SERVICE</strong></h5>
						<hr style="width: 30%;" class="pull-left"><br><br>	
						<p>Shipping and Return</p>
						<p>Track Order</p>
						<p>Shopping Cart</p>
						<p>Wishlist</p>
						<p>Frequently Questions</p>
					</div>
					<div class="col-sm-4 col-md-2 padding-tb-15 wow fadeInDown" data-wow-delay="0.2s">
						<h5 class="textcenter footer-heads"><strong>MY ACCOUNT</strong></h5>
						<hr style="width: 30%;" class="pull-left"><br><br>	
						<p>Shipping and Return</p>
						<p>Track Order</p>
						<p>Shopping Cart</p>
						<p>Wishlist</p>
						<p>Frequently Questions</p>
					</div>
					<div class="col-sm-4 col-md-2 padding-tb-15 wow fadeInDown" data-wow-delay="0.3s">
						<h5 class="textcenter footer-heads"><strong>CATEGORIES</strong></h5>
						<hr style="width: 30%;" class="pull-left"><br><br>	
						<p>Shipping and Return</p>
						<p>Track Order</p>
						<p>Shopping Cart</p>
						<p>Wishlist</p>
						<p>Frequently Questions</p>
					</div>
					<div class="col-sm-12 col-md-3 padding-tb-15 wow fadeInDown" data-wow-delay="0.4s">
						<h5 class="textcenter"><strong>INSTAGRAM FEEDS</strong></h5>
						<hr style="width: 30%;" class="pullleft"><br><br>
						<div class="col-sm-12 col-md-10 no-padding-lf">
							<div class="col-xs-4 col-sm-4 col-md-4 no-padding-lf insta-pic"><img src="img/category/1.png" class="img-responsive"></div>
							<div class="col-xs-4 col-sm-4 col-md-4 no-padding-lf insta-pic"><img src="img/category/2.png" class="img-responsive"></div>
							<div class="col-xs-4 col-sm-4 col-md-4 no-padding-lf insta-pic"><img src="img/category/3.png" class="img-responsive"></div>
							<div class="col-xs-4 col-sm-4 col-md-4 no-padding-lf insta-pic"><img src="img/category/4.png" class="img-responsive"></div>
							<div class="col-xs-4 col-sm-4 col-md-4 no-padding-lf insta-pic"><img src="img/category/5.png" class="img-responsive"></div>
							<div class="col-xs-4 col-sm-4 col-md-4 no-padding-lf insta-pic"><img src="img/category/6.png" class="img-responsive"></div>	
						</div>
					</div>
				</div>		
			</div>
		</div>
	</section>
<footer style="padding:25px 15px; border-top: 1px solid #000; background-color: #2c3e50; color: white;" class="text-center" id="footer">&copy; Copyright 2013-2015 UniqueStore</footer>

<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/sidebarEffects.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".product-slider-inner>.item:first-of-type").addClass("active");
		$(".card-slider>.item:first-of-type").addClass("active");
		$(".main-carousel-inner>.item:first-of-type").addClass("active");
		
	});
</script>
<script>

$('.carousel[data-type="multi"] .item').each(function(){
	
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++){
    next=next.next();
    if (!next.length){
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
</script>

<script type="text/javascript">

	jQuery("#size").change(function(){
		var available = jQuery('#size option:selected').data("available");
		jQuery('#available').val(available); 
	});

	function add_to_cart(){
		var size = jQuery('#size').val();
		var quantity = jQuery('#quantity').val();
		var available = jQuery('#available').val();
		var err = '';
		var data = jQuery('#add_product_form').serialize();
		// alert(data);
		if (size == '' || quantity == '' || quantity == '0') {
			// alert(quantity);
			// alert(available);
			err +='<p class="text-danger text-center">You must choose a size and quantity.</p>';
			jQuery('#modal_errors').html(err);
			return;
		}
		if (quantity > available) {
			 alert(quantity);
			 alert(available);
			err +='<p class="text-danger text-center">There are only '+available +'available.</p>';
			jQuery('#modal_errors').html(err);
			return;
		}else{
			jQuery.ajax({
				url : '/UniqueStore/adminnew/parsers/add_cart.php',
				method : 'post',
				data : data,
				success : function(){ 
					location.reload();
				 },
				error : function(){ alert("something went wrong");}   
			});  
		}
	}

	function update_cart(mode,edit_id,edit_size){
		

		// alert(mode);
		// alert(edit_id);
		// alert(edit_size);
		var data = {"mode" : mode, "edit_id" : edit_id, "edit_size" :edit_size};
		// alert(data);
		 jQuery.ajax({
			url : '/UniqueStore/adminnew/parsers/update_cart.php',
			method : "post",
			data : data,
			success : function(){ location.reload(); },
			error :function(){ alert("something went wrong");},
		});
	}



</script>
</body>
</html>
