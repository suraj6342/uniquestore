<!doctype html>
<html lang="en" >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<script src="js/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<title>UniqueStore - Ecommerce Template</title>
</head>
<body>
	<!-- mobile nav -->
	<div class ="container no-pad-lf hidden-sm hidden-md hidden-lg">
	 	<div class="row mobile-nav-container">
	 		<div class="col-xs-12">
	 			 <div class="col-xs-2 mobile-nav"><button class="menu-btn"><span class="glyphicon glyphicon-user"></span></button></div>
	 			 <div class="col-xs-2 mobile-nav"><button class="menu-btn"><span class="glyphicon glyphicon-user"></span></button></div>
	 			 <div class="col-xs-2 mobile-nav"><button class="menu-btn"><span class="glyphicon glyphicon-user"></span></button></div>
	 			 <div class="col-xs-2 mobile-nav"><button class="menu-btn"><span class="glyphicon glyphicon-heart"></span></button></div>
	 			 <div class="col-xs-2 mobile-nav"><a href="/UniqueStore/cart.php" class="menu-btn navbar-cart-btn-xs"><span class="glyphicon glyphicon-shopping-cart"></span></a></div>
	 		</div>
		</div>
		<div class="row mobile-logo">
				<div class=" text-center"><a href="/UniqueStore/index.php"><img src="img/cd-logo.svg" alt="Logo"></a></div>
		</div>
	</div>
	<!-- end -->
<header class="cd-auto-hide-header hidden-xs">
	<!-- laptop and above navbar -->
	<nav class="cd-primary-nav hidden-xs">
		<div class="logo"><a href="/UniqueStore/index.php"><img src="img/cd-logo.svg" alt="Logo"></a></div>
		<div><p class="contact">Contact - 98989898998</p></div>
	</nav> <!--.cd-primary-nav -->

	<nav class="cd-secondary-nav no-pad-bottom">
		<ul>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-2 text-center"><a href="/UniqueStore/index.php"><img src="img/cd-logo.svg" alt="Logo"></a></div>
				<div class="col-md-1"></div>
				<div class="col-sm-8 col-md-6 text-center hidden-xs hidden-sm">
					<form class="form-inline">
						<div class="form-inline">
							<div class="col-md-10 col-md-offset-1 form-group search-form">
								
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-3 col-md-3 right-align hidden-xs hidden-sm">
					<button class="menu-btn user-btn"><span class="glyphicon glyphicon-user"></span></button>
					<button class="menu-btn heart-btn"><span class="glyphicon glyphicon-heart"></span></button>
					<a href="/UniqueStore/cart.php" class="menu-btn navbar-cart-btn"><span class="glyphicon glyphicon-shopping-cart"></span></a>
				</div>
			</div>
		</div>
		</ul>
	</nav> <!-- .cd-secondary-nav -->
	<!-- end -->
	
	<!-- tablet navbar -->
	<nav class="cd-secondary-nav hidden-md hidden-lg hidden-xs no-pad-top">
		<ul>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 hidden-xs">
					<form class="form-inline">
						<div class="form-inline">
							<div class="form-group search-form">
								<div class="pull-left input-group">
								
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-4 hidden-xs text-right">
					<button class="menu-btn"><span class="glyphicon glyphicon-user"></span></button>
					<button class="menu-btn"><span class="glyphicon glyphicon-heart"></span></button>
					<a href="/UniqueStore/cart.php" class="menu-btn navbar-cart-btn"><span class="glyphicon glyphicon-shopping-cart"></span></a>
				</div>
			</div>
		</div>
		</ul>
	</nav> <!-- .cd-secondary-nav -->
	<!-- end -->
</header> <!-- .cd-auto-hide-header -->
