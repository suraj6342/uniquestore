<?php 
$db =mysqli_connect('localhost','root','','uniquestore');
if(mysqli_connect_errno())
{
	echo 'failed connnection'.mysqli_connect_error() ;
	die();
}

$mainslidersql = "SELECT * FROM products WHERE productslider = '1' ";
$mainsliderquery = $db->query($mainslidersql);


?>

<main class="cd-main-content sub-nav">
	<div>
		<div id="carousel-example-generic" class="carousel slide main-slider" data-ride="carousel" data-interval="10000000">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner main-carousel-inner" role="listbox">
		     <?php while($product = mysqli_fetch_assoc($mainsliderquery)): ?>
		    <div class="item">
		      <img src="img/slider/back1.jpg" alt="..." class="img-responsive main-slider-img">
		      <div class="carousel-caption main-carousel-caption">
		      		<div class="row">
		      		<div class="col-xs-6 col-sm-6 col-md-6 left-tab">
		      			<h1 class="text-left font-24"><?=$product['title'];?></h1>
				        <h3 class="text-left font-14"><?=$product['description'];?></h3>
				        <div class="margin-t-30">
				        	<a href="single-product.php?productid=<?=$product['id'];?>" class="menu-btn anchor">Purchase</a>
				        </div>
		      		</div>
		      		<div class="col-xs-6 col-sm-6 col-md-6 margin-t-30">
				        <img src="<?=$product['image'];?>" class="img-responsive">
		      		</div>
		      		</div>
		      </div>
		    </div>
			<?php endwhile; ?>
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>

	</div>
</main> <!-- .cd-main-content -->
