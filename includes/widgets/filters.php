<?php 

$cat_id = ((isset($_REQUEST['cat']))?sanitize($_REQUEST['cat']):'');
$price_sort = ((isset($_REQUEST['price_sort']))?sanitize($_REQUEST['price_sort']):'');
$min_price = ((isset($_REQUEST['min_price']))?sanitize($_REQUEST['min_price']):'');
$max_price = ((isset($_REQUEST['max_price']))?sanitize($_REQUEST['max_price']):'');
$b = ((isset($_REQUEST['brand']))?sanitize($_REQUEST['brand']):'');
$brandQ = $db->query("SELECT * FROM brand ORDER BY brand");
?>
<div class="category-menu padding-25-15 margin-tb-20">
<h3 class="">Search By :</h3>
<h4 class="mt-15">Price</h4>
<form action="search.php" method="post">
	<input type="hidden" name="cat" value="<?=$cat_id;?>">
	<input type="hidden" name="price_sort" value="0">
	<input type="radio" name="price_sort" class="mt-15" value="low"<?=(($price_sort == 'low')?' checked':'');?>> Low to High<br>
	<input type="radio" name="price_sort" class="mt-15" value="high"<?=(($price_sort == 'high')?' checked':'');?>> High to Low <br>
	<input type="text" name="min_price" class="price-range mt-15 form-control" placeholder="min $" value="<?=$min_price;?>"><br><span class="mt-5">To</span>
	<input type="text" name="max_price" class="price-range mt-5 form-control" placeholder="max $" value="<?=$max_price;?>">
	<h4 class="">Brand</h4>
	<input type="radio" name="brand" value=""<?=(($b == '')?' checked':'');?>> All<br>
	<?php while ($brand = mysqli_fetch_assoc($brandQ)): ?>
	<input type="radio" name="brand" value="<?=$brand['id'];?>"<?=(($b == $brand['id'])?' checked':'');?>> <?=$brand['brand'];?><br>
	<?php endwhile; ?>
	<input type="submit" value="Search" class="btn btn-xs btn-primary mt-15">
</form>
</div>