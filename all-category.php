<?php 
require'core/db.php';
include'includes/top-header.php';  

$pcatid = $_REQUEST['catid'];
$pcatsql = "SELECT * FROM categories WHERE id = '$pcatid'";
$pcatquery = $db->query($pcatsql);
$pcat = mysqli_fetch_assoc($pcatquery);

$childcatsql = "SELECT * FROM categories WHERE parent = '$pcatid'";
$childcatquery = $db->query($childcatsql);


?>

<section class="page-header col-md-12">
	<div class="container-fuild">
		<div class="row">
			<div class="col-xs-12 col-md-12 no-padding-lf padding-b-100">
				<div class="col-xs-12 col-md-12 backheader">
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 cate-frontheader">
					<h1 class="padding-l-10"><?=$pcat['category'];?></h1>
				
					<?php while($childcat = mysqli_fetch_assoc($childcatquery)):?>
					<div class="col-sm-4 col-md-3 category-box wow slideInRight" >
						<div style="background-image: url(img/1.png);" class="box">
							<a href="category.php?childcat=<?=$childcat['id'];?>"><h3 class="cate-head"><?=$childcat['category'];?></h3></a>
						</div>
					</div>
					<?php endwhile; ?>


 					<!-- <div class="col-sm-4 col-md-3 category-box wow slideInRight" data-wow-delay="0.1s">
						<div style="background-image: url(img/1.png);" class="box">
							<h3 class="cate-head">category name</h3>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 category-box wow slideInRight" data-wow-delay="0.2s">
						<div style="background-image: url(img/1.png);" class="box">
							<h3 class="cate-head">category name</h3>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 category-box wow slideInRight" data-wow-delay="0.3s">
						<div style="background-image: url(img/1.png);" class="box">
							<h3 class="cate-head">category name</h3>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 category-box wow slideInRight" data-wow-delay="0.4s">
						<div style="background-image: url(img/1.png);" class="box">
							<h3 class="cate-head">category name</h3>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 category-box wow slideInRight" data-wow-delay="0.5s">
						<div style="background-image: url(img/1.png);" class="box">
							<h3 class="cate-head">category name</h3>
						</div>
					</div> 
					<div class="col-sm-4 col-md-3 category-box wow slideInRight" data-wow-delay="0.6s">
						<div style="background-image: url(img/1.png);" class="box">
							<h3 class="cate-head">category name</h3>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</section>

<?php
include'includes/footer.php';  

?>
