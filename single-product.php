<?php 
require'core/db.php';
include'includes/top-header.php';

$id = $_REQUEST['productid'];
$id1 = (int)$id;

$sql = "SELECT * FROM products WHERE id = '$id1'";
$result = $db->query($sql);
$product = mysqli_fetch_assoc($result);


$brand_id = $product['brand'];
$sql = "SELECT * FROM brand WHERE id = '$brand_id'";
$bquery = $db->query($sql);
$brand = mysqli_fetch_assoc($bquery);
$size_array = $product['sizes'];


$childID = $product['categories'];
$catsql  = "SELECT * FROM categories Where id = $childID";
$result = $db->query($catsql);
$child = mysqli_fetch_assoc($result);
$parentID = $child['parent'];
$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
$presult = $db->query($psql);
$parent = mysqli_fetch_assoc($presult);
$category = $parent['category'].'-'.$child['category'];



?>
<!-- product description -->
<section class="page-header col-md-12">
	<div class="container-fuild">
		<div class="row ">
			<div class="col-xs-12 col-md-12 no-pad-lf">
				<div class="col-xs-12 col-md-12 backheader">
					
				</div>
				<div class="col-xs-10 col-sm-10 col-md-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-2 all-p-frontheader">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<span id="modal_errors" class="bg-danger col-sm-6"></span>
					</div>
					<div class="col-md-1 thumb-image no-padding-lf">
						<div class="thumb ">
							<img src="" class="img-responsive">		
						</div>
						<div class="thumb ">
							<img src="" class="img-responsive">		
						</div>
						<div class="thumb ">
							<img src="" class="img-responsive">		
						</div>
						<div class="thumb ">
							<img src="" class="img-responsive">		
						</div>
						<div class="thumb ">
							<img src="" class="img-responsive">		
						</div>
					</div>
					<div class="col-md-4 main">
						<?php $photos = explode(',',$product['image']);
						foreach($photos as $photo): ?>
							<img src="<?=$photo;?>" alt="<?= $product['title'];?>" class="img-responsive">
						<?php endforeach; ?>
					</div>	
					<div class="col-md-7">
						<div class="col-md-12">
							<h3><?=$product['title'];?></h3>
							<h4><?=$category;?></h4>
						</div>
						<div class="col-md-12">
							<p><?=$product['description'];?></p>
						</div>
						<form action="add_cart.php" method="post" id="add_product_form">
						<input type="hidden" name="product_id" id="product_id" value="<?=$id;?>">
						<input type="hidden" name="available" id="available" value="">
						<div class="col-md-12">
							<h4>Sizes & Quantity Available</h4>
							<select name="size" id="size" class="form-control">
									<option value=""></option>
									<?php 
										$size_Array =array();
										$sizesArray = explode(',', $size_array);
										foreach ($sizesArray as $ss ) {
											$s =explode(':', $ss);
											$size= $s[0];
											$available = $s[1];
											if ($available > 0) {
												echo '<option value="'.$size.'" data-available="'.$available.'">'.$size.' ('.$available.' Available)</option>';		
											}
												
										}
									?>
							</select>
						</div>
						<div class="col-md-12 margin-t-30">
							<div class=" box-shadow price-tab">
							<h3 class=""><?=money($product['price']);?></h3><h5 class=""><s><?=money($product['list-price']);?></s></h5>
							<input type="number" min="1" id="quantity" name="quantity" placeholder="quantity" class="input-btn col-md-2">
							<button style="margin-left: 10px;" class="input-btn" onclick="add_to_cart();return false;">Add to cart <span class="glyphicon glyphicon-cart"></span></button>
							</div>
						</div>
						</form>
					</div>
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 description-tab">
				
				</div>
			</div>
		</div>
	</div>
</section>

<div class="related-product">
	<div class="container-fuild">
		<div class="row brand-row">
			<div style="background-color: green;" class="col-md-12 text-center padding-tb-40">
				<h1>Realted Product</h1>
				<div class="col-md-12 slider">
					<button>slider</button>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include'includes/footer.php';  
?>
