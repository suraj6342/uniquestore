<?php 
require'core/db.php';
include'includes/top-header.php';  

$items = array();

if($cart_id != '') {
	$cartQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
	$result = mysqli_fetch_assoc($cartQ);
	$items = json_decode($result['items'],true);
	$i = 1;
	$sub_total = 0;
	$item_count = 0;

}



?>

<div class="col-md-12 cart-section">
	<div class="row pad-lf-25">
		<h2 class="text-center">My Shopping Cart</h2><hr>
		<?php if($cart_id == ''): ?>
			<div class="bg-danger">
				<p class="text-center text-danger">
					Your shopping cart is empty!
				</p>
			</div>
		<?php else: ?>
			<div style="overflow-x: auto;">
			<table class="table table-striped">
				<thead>
					<th>#</th>
					<th>Item</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Size</th>
					<th>Image</th>
					<th>Sub Total</th>
				</thead>
				<tbody>
					<?php 
					$s = array();
						foreach ($items as $item) {
							$product_id = $item['id'];
							$sArray = array();

							$productQ = $db->query("SELECT * FROM products WHERE id ='{$product_id}'");
							$product = mysqli_fetch_assoc($productQ);
							$sArray = explode(',', $product['sizes']);
							foreach ($sArray as $sizeString) {
							 	$s = explode(':', $sizeString);
							 	if ($s[0] == $item['size']) {
							 		$available = $s[1];
							 	}
							 } 
							 ?>
							 <tr>
							 	<td><?=$i;?></td>
							 	<td><?=$product['title'];?></td>
							 	<td><?=money($product['price']);?></td>
							 	<td>

							 	<button class="btn menu-btn" onclick="update_cart('removeone','<?=$product['id'];?>','<?=$item['size'];?>');"> - </button>
							 	&nbsp<?=$item['quantity'];?>&nbsp
							 	<?php if($item['quantity'] < $available): ?> 	 
							 	<button class="btn menu-btn" onclick="update_cart('addone','<?=$product['id'];?>','<?=$item['size'];?>');"> + </button>
							 	<?php else: ?>
							 		<span class="text-danger">Max</span>
							 	<?php endif; ?>
							 	
							 	</td>
							 	<td><?=$item['size'];?></td>
							 	<?php $photos = explode(',',$product['image']);?>
							 	<td><img src="<?=$photos[0];?>" height="80px" width="80px" ></td>
							 	<td><?=money($item['quantity'] * $product['price']);?></td>
							 </tr>

							 <?php
							 $i++; 
							 $item_count += $item['quantity'];
							 $sub_total += ($product['price'] * $item['quantity']);

							}
							$tax = TAXRATE * $sub_total;
							$tax = number_format($tax,2);
							$grand_total = $tax + $sub_total;

							
							?>
				</tbody>
			</table>
			</div>
			<hr>
			<h2 class="text-center">Grand Total</h2><hr>
			<table class="table table-striped">
				
					<th>Total Items</th>
					<th>Sub Total</th>
					<th>Tax</th>
					<th>Grand Total</th>
				</thead>
				<tbody>
					<tr>
						<td><?=$item_count;?></td>
						<td><?=money($sub_total);?></td>
						<td><?=money($tax);?></td>
						<td class="bg-success"><?=money($grand_total);?></td>
					</tr>
				</tbody>
			</table>
<div class="checkout-btn">
<button style="margin-right: 15px;" type="button" class="btn btn-lg menu-btn pull-right" data-toggle="modal" data-target="#checkoutmodal"><span class="glyphicon glyphicon-shopping-cart"></span> Check Out >></button>
</div>


<!-- modal -->
	<div class="modal fade" id="checkoutmodal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center" id="checkoutmodalLabel">Shipping Address</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="thankyou.php" method="post" id="payment-form">
							<span class="bg-danger" id="payment-errors"></span>
							<input type="hidden" name="tax" value="<?=$tax;?>">
							<input type="hidden" name="sub_total" value="<?=$sub_total;?>">
							<input type="hidden" name="grand_total" value="<?=$grand_total;?>">
							<input type="hidden" name="cart_id" value="<?=$cart_id;?>">
							<input type="hidden" name="description" value="<?=$item_count.' item'.(($item_count>1)?'s':'').' from ecommerce shop.';?>">
							<div id="step1" style="display: block;">
								<div class="form-group col-md-6">
									<label for="full_name">Full Name:</label>
									<input type="text" name="full_name" id="full_name" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="email">Email:</label>
									<input type="email" name="email" id="email" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="street">Street Address:</label>
									<input type="text" name="street" id="street" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="street2">Street Address 2:</label>
									<input type="text" name="street2" id="street2" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="city">City:</label>
									<input type="text" name="city" id="city" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="state">State:</label>
									<input type="text" name="state" id="state" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="zip_code">Zip Code:</label>
									<input type="text" name="zip_code" id="zip_code" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="country">Country:</label>
									<input type="text" name="country" id="country" class="form-control">
								</div>
							</div>
							<div id="step2" style="display: none;">
								<div class="form-group col-md-3">
									<label for="name">Name on Card :</label>
									<input type="text" id="name" name="name" class="form-control">
								</div>
								<div class="form-group col-md-3">
									<label for="number">Card Number :</label>
									<input type="text" id="number" name="number" class="form-control">
								</div>
								<div class="form-group col-md-1 ">
									<label for="number">CCV :</label>
									<input type="text" id="number" name="number" class="form-control padding-5">
								</div>
								<div class="form-group col-md-2">
									<label for="expire">Expire Month :</label>
									<select id="exp-month" class="form-control">
										<option value=""></option>
										<?php for($i=1 ; $i < 13 ; $i++):?>
										<option value="<?=$i;?>"><?=$i;?></option>
										<?php endfor; ?>
									</select>
								</div>
								<div class="form-group col-md-2">
									<label for="exp-year">Expire Year</label>
									<select id="exp-year" class="form-control">
										<option value=""></option>
										<?php $yr = date("Y"); ?>
										<?php for ($i=0; $i < 11; $i++):  ?>
										<option value="<?=$yr+$i;?>"><?=$yr+$i;?></option>
										<?php endfor; ?>
									</select>
								</div>
							</div>
						
					</div>
				</div>
					<div class="modal-footer">
						<button type="button" class="btn menu-btn" data-dismiss="modal" >Close</button>
						<button type="button" class="btn menu-btn" onclick="check_address();" id="next_button">Next >></button>
						<button type="button" class="btn menu-btn" onclick="back_address();" id="back_button" style="display: none;"><< Back </button>
						<button type="submit" class="btn menu-btn" id="check_out_button" style="display: none;">Check Out >></button>
						</form>
					</div>
			</div>
		</div>
	</div>



		<?php endif; ?>
	</div>
<script type="text/javascript">

function back_address(){
	jQuery('#payment-errors').html("");
	jQuery('#step1').css("display","block");
	jQuery('#step2').css("display","none");
	jQuery('#next_button').css("display","inline-block");
	jQuery('#back_button').css("display","none");
	jQuery('#check_out_button').css("display","none");
	jQuery('#checkoutmodalLabel').html("Shipping Address");

}
	
function check_address(){
	var full_name = jQuery('#full_name').val();
	var email = jQuery('#email').val();
	var street = jQuery('#street').val();
	var street2 = jQuery('#street2').val();
	var city = jQuery('#city').val();
	var state = jQuery('#state').val();
	var zip_code = jQuery('#zip_code').val();
	var country = jQuery('#country').val();
	
	var data = {
		"full_name" : full_name,
		"email" : email,
		"street" : street,
		"street2" : street2,
		"city" : city,
		"state" : state,
		"zip_code" : zip_code,
		"country" : country
	};
	jQuery.ajax({
		url : '/UniqueStore/adminnew/parsers/check_address.php',
		method : 'POST',
		data : data,
		success : function(data){ 
			if(data == 0 ) {
				jQuery('#payment-errors').html("");
				jQuery('#step1').css("display","none");
				jQuery('#step2').css("display","block");
				jQuery('#next_button').css("display","none");
				jQuery('#back_button').css("display","inline-block");
				jQuery('#check_out_button').css("display","inline-block");
				jQuery('#checkoutmodalLabel').html("Enter Your Card details");

			}else{
				jQuery('#payment-errors').html(data);
			}
			
		},
		error : function(){ alert("something went wrong");}, 
	});
}

</script>


<?php
include'includes/footer.php';  

?>
