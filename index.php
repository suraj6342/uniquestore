
<?php 
require'core/db.php';
include'includes/top-header.php';  
include'includes/header-partial.php';  

$categorysql = "SELECT * FROM categories WHERE parent = '0' ";
$categoryquery = $db->query($categorysql);
$categoryquery1 = $db->query($categorysql);

$featuresql = "SELECT * FROM products WHERE featured = '1' ";
$featurequery = $db->query($featuresql);
$cardslidersql = "SELECT * FROM products WHERE cardproduct = '1' ";
$cardsliderquery = $db->query($cardslidersql);
$newproductsql = "SELECT * FROM products WHERE newproduct = '1' ";
$newproductquery = $db->query($newproductsql);
$trendingsql = "SELECT * FROM products WHERE trendingproduct = '1' ";
$trendingquery = $db->query($trendingsql);
$salesql = "SELECT * FROM products WHERE saleproduct = '1' ";
$salequery = $db->query($salesql);
$tabuppersql = "SELECT * FROM products WHERE tabproductupper = '1' ";
$tabupperquery = $db->query($tabuppersql);
$tablowersql = "SELECT * FROM products WHERE tabproductlower = '1' ";
$tablowerquery = $db->query($tablowersql);

?>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 all-category-tab">
			<div class="col-xs-12 col-sm-12 col-md-11 col-md-offset-1 category-col wow bounceInLeft">
				<h3 class="pull-left">All Categories</h3><br>
				<br>
				<hr>
				<ul class="list-group">
  					<?php while($category = mysqli_fetch_assoc($categoryquery)):	
					?>
					<a href="all-category.php?catid=<?=$category['id'];?>"><li class="list-group-item">
					<?=$category['category'];?>
					<span class="caret pull-right"></span> </li></a>
				<?php endwhile ;?>
				</ul>
				 <div class="col-xs-12 col-sm-12 col-md-12 view-more"><a href="all-product.php" class="pull-right menu-btn">Show all Products</a></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 text-center all-category-tab">
			<div class="col-xs-12 col-sm-12 no-padding-lf">
				
				<?php while($tabupperproduct = mysqli_fetch_assoc($tabupperquery)): ?>
				<div class="col-xs-6 col-sm-6 col-md-6 no-padding-lf pad-5 wow slideInRight">
					<div class="col-sm-12 col-md-12 no-padding-lf new-items">
						<div style="background-image: url(<?=$tabupperproduct['image'];?>);" class="col-sm-12 col-md-12 new-items-inner">
							<h4 class="new-item-head font-12"><?=$tabupperproduct['title'];?></h4>
							<a href="single-product.php?productid=<?=$tabupperproduct['id'];?>" class="menu-btn pull-right">Shop now</a>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
			</div>
		</div>

	</div>
</div>
<section class="feature-list padding-tb-40 wow fadeInDown">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 padding-tb-40-xs">
				<div class="col-sm-5 col-md-2 padding-tb-15 col-md-offset-1 first-tab">
					<div>
						<img src="">
						<h4>Free Home Delivery</h4>
						<p>for all products</p>	
					</div>
				</div>
				<div class="col-sm-1 col-md-1 line  hidden-xs">
					<img src="img/line.png" class="img-responsive">
				</div>
				<div class="col-sm-5 col-md-2 padding-tb-15">
					<div>
						<img src="">
						<h4>10 Days Return</h4>
						<p>no longer wait for return</p>	
					</div>
				</div>
				<div class="col-md-1 line hidden-sm hidden-xs">
					<img src="img/line.png" class="img-responsive">
				</div>
				<div class="col-sm-5 col-md-2 padding-tb-15 third-tab">
					<div>
						<img src="">
						<h4>Discount Coupons</h4>
						<p>sign up and get coupons</p>	
					</div>
				</div>
				<div class="col-sm-1 col-md-1 line hidden-xs">
					<img src="img/line.png" class="img-responsive">
				</div>
				<div class="col-sm-5 col-md-2 padding-tb-15">
					<div>
						<img src="">
						<h4>Safe Shopping</h4>
						<p>secure payment portals </p>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="products-tab">
	<div class="container-fluid padding-tb-40 xs-pad ">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 padding-tb-40">	
				<div class="slider-box col-xs-12 col-sm-10 col-md-4 col-lg-4 col-sm-offset-1 col-md-offset-0 text-center border-radius-shadow wow slideInRight">
					<h3 class="text-center deal-head">Deal of the day</h3>
					<div class="col-md-12 slider-box-inner no-padding-lf border-radius-shadow">
						<div id="carousel-example-generic-1" class="carousel slide border-radius-shadow" data-ride="carousel" data-interval="3000">
						  <h4 class="text-center counter">14 days:21 hour:13 mins:12 secs</h4>
						  <!-- Indicators -->
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner product-slider card-slider border-radius-shadow" role="listbox">
						   <?php while($cardproduct = mysqli_fetch_assoc($cardsliderquery)): 
						    $childID = $cardproduct['categories'];
							$catsql  = "SELECT * FROM categories Where id = $childID";
							$result = $db->query($catsql);
							$child = mysqli_fetch_assoc($result);
							$parentID = $child['parent'];
							$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
							$presult = $db->query($psql);
							$parent = mysqli_fetch_assoc($presult);
							$category = $parent['category'].'-'.$child['category'];
						   ?>
							    <div id="slider-item" class="item">
							      <a href="single-product.php?productid=<?=$cardproduct['id'];?>"><img src="<?=$cardproduct['image'];?>" alt="..." class="img-responsive"></a>
							      <h3 class="text-center"><?=$cardproduct['title'];?></h3>
							      <h5 class="text-center"><?=$category;?></h5>
							      <h3 class="text-center"><?=money($cardproduct['price']) ;?></h3>
							      
							    </div>
							<?php endwhile; ?>
						  </div>

						  <!-- Controls -->
						 
						</div>
					</div>
					<div class="col-md-12 slider-controls no-padding-lf">
						<div class="col-md-6 no-padding-lf">
							<a class="menu-btn pull-left" href="#carousel-example-generic-1" role="button" data-slide="prev">Prev</a>
						</div>
						<div class="col-md-6 no-padding-lf">
							<a class="menu-btn pull-right" href="#carousel-example-generic-1" role="button" data-slide="next">Next</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 text-center new-product-tab wow slideInLeft">
					<div>
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><h4>New Product</h4></a></li>
					    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><h4>Trending</h4></a></li>
					    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><h4>Sale Product</h4></a></li>
					  </ul>
					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="home">
					    	<div class="col-xs-12 col-sm-12 col-md-12">
					    		<?php while($newproduct = mysqli_fetch_assoc($newproductquery)):
					    		$childID = $newproduct['categories'];
								$catsql  = "SELECT * FROM categories Where id = $childID";
								$result = $db->query($catsql);
								$child = mysqli_fetch_assoc($result);
								$parentID = $child['parent'];
								$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
								$presult = $db->query($psql);
								$parent = mysqli_fetch_assoc($presult);
								$category = $parent['category'].'-'.$child['category'];	

					    		?>
					    		<div class="col-xs-6 col-sm-4 col-md-3 new-product">
					    			<a href="single-product.php?productid=<?=$newproduct['id'];?>" ><img src="<?=$newproduct['image'];?>" class="img-responsive"></a>
					    			<h5 class="text-center"><?=$newproduct['title'];?></h5>
					    			<h6 class="text-center"><?=$category;?></h6>
					    			<h5 class="text-center"><?=money($newproduct['price']);?></h5>
					    			
					    		</div>
					    		<?php endwhile;?>

					    	</div>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="profile">
					    	<div class="col-md-12">
					    		<?php while($trending = mysqli_fetch_assoc($trendingquery)):
					    		$childID = $trending['categories'];
								$catsql  = "SELECT * FROM categories Where id = $childID";
								$result = $db->query($catsql);
								$child = mysqli_fetch_assoc($result);
								$parentID = $child['parent'];
								$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
								$presult = $db->query($psql);
								$parent = mysqli_fetch_assoc($presult);
								$category = $parent['category'].'-'.$child['category'];	

					    		?>
					    		<div class="col-xs-6 col-sm-4 col-md-3 new-product">
					    			<a href="single-product.php?productid=<?=$trending['id'];?>"><img src="<?=$trending['image'];?>" class="img-responsive"></a>
					    			<h5 class="text-center"><?=$trending['title'];?></h5>
					    			<h6 class="text-center"><?=$category;?></h6>
					    			<h5 class="text-center"><?=money($trending['price']);?></h5>
					    			
					    		</div>
					    		<?php endwhile;?>

					    	</div>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="messages">
					    	<div class="col-md-12">
					    		<?php while($sale = mysqli_fetch_assoc($salequery)):
					    		$childID = $sale['categories'];
								$catsql  = "SELECT * FROM categories Where id = $childID";
								$result = $db->query($catsql);
								$child = mysqli_fetch_assoc($result);
								$parentID = $child['parent'];
								$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
								$presult = $db->query($psql);
								$parent = mysqli_fetch_assoc($presult);
								$category = $parent['category'].'-'.$child['category'];	

					    		?>
					    		<div class="col-xs-6 col-sm-4 col-md-3 new-product">
					    			<a href="single-product.php?productid=<?=$cardproduct['id'];?>"><img src="<?=$sale['image'];?>" class="img-responsive"></a>
					    			<h5 class="text-center"><?=$sale['title'];?></h5>
					    			<h6 class="text-center"><?=$category;?></h6>
					    			<h5 class="text-center"><?=money($sale['price']);?></h5>
					    		</div>
					    		<?php endwhile;?>

					    	</div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section style="background-color: floralwhite;" class="featured-product wow fadeInDown">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center padding-tb-40">
				<h1 class="text-center featured-head">Featured Products</h1>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">	
				<div class="col-md-12">
					<div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="10000000" id="myCarousel">
					  <div class="carousel-inner product-slider-inner">
					  <?php while($featurepro = mysqli_fetch_assoc($featurequery)): 

						   $childID = $featurepro['categories'];
							$catsql  = "SELECT * FROM categories Where id = $childID";
							$result = $db->query($catsql);
							$child = mysqli_fetch_assoc($result);
							$parentID = $child['parent'];
							$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
							$presult = $db->query($psql);
							$parent = mysqli_fetch_assoc($presult);
							$category = $parent['category'].'-'.$child['category'];
					  ?>

					    <div class="item">
					      <div class="col-md-2 col-sm-6 col-xs-12 text-center">
					      	<a href="single-product.php?productid=<?=$featurepro['id'];?>"><img src="<?=$featurepro['image'];?>" class="img-responsive"></a>
					    	<h4 class="text-center"><?=$featurepro['title'];?></h4>
					    	<h5 class="text-center"><?=$category;?></h5>
					    	<h4 class="text-center"><?=money($featurepro['price']);?></h4>
					    	
					      </div>
					    </div>
					  <?php endwhile; ?>
					  </div>
					  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
					  <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	
</section>

<section style="background-color: floralwhite;" class="shop-cards">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="col-xs-12 col-sm-12 col-md-12 padding-tb-40 wow slideInLeft">
					<?php while($tablowerproduct = mysqli_fetch_assoc($tablowerquery)): ?>
					<div class="col-sm-4 col-md-4 new-items">
						<div style="background-image: url(<?=$tablowerproduct['image'];?>); background-size: cover;" class="col-sm-12 col-md-12 new-items-inner">
							<a href="single-product.php?productid=<?=$tablowerproduct['id'];?>" class="new-item-head font-12 block-display"><?=$tablowerproduct['title'];?></a>
							<button class="menu-btn pull-right margin-t-15">Shop now</button>
						</div>
					</div>
				<?php endwhile; ?>
				</div>
				
			</div>
		</div>
	</div>
</section>
<section class="brands">
	<div class="container-fluid no-padding-lf">
		<div class="row brand-row wow pulse">
			<div class="col-sm-12 col-md-12 padding-tb-50 hidden-xs">
				<div class="col-sm-1 col-md-1 pad-5-lf col-md-offset-1 col-sm-offset-1"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
				<div class="col-sm-1 col-md-1 pad-5-lf"><img src="img/4.jpg" class="img-responsive"></div>
			</div>
		</div>
		<!-- mobile brand slider -->
		<div class="row no-margin-lf hidden-sm hidden-md hidden-lg ">
			<div class="col-sm-12 col-md-12 padding-tb-40">
				<div id="carousel-example-generic-4" class="carousel slide brand-slider wow pulse" data-ride="carousel" data-interval="3000">
				  <!-- Indicators -->
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="img/4.jpg" alt="..." class="img-responsive">
				    </div>
				    <div class="item">
				      <img src="img/4.jpg" alt="..." class="img-responsive">
				    </div>
				    <div class="item">
				      <img src="img/4.jpg" alt="..." class="img-responsive">
				    </div>
				   
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic-4" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic-4" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>
	</div>
</section>
<section style="background-color: #f5b53c;" class="subscribe">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center padding-tb-40 wow slideInRight">
				<div class="col-md-6 col-md-offset-3 margin-t-15">
					<h1>Subscribe Newsletter</h1>
					<hr>
					<h4 class="margin-t-15">get latest update and newsletters latest trendy deals and discount coupons.</h4>
					<input type="text" name="" class="form-control">
				</div>
			</div>
		</div>
	</div>
</section>
<?php
include'includes/footer.php';  

?>

	
