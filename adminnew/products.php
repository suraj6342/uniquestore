<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/UniqueStore/core/db.php';

$result = is_logged_in();

//echo $result;
if ($result == '0' ) {
	login_error_redirect();
}
include'includes/head.php';
include'includes/navigation.php';

//delete product
if (isset($_GET['delete'])) {
	$id = sanitize($_GET['delete']);
	$db->query("UPDATE products SET deleted = 1 WHERE id = '$id'");
	header('Location:products.php');
}


$dbpath = ''; 

if (isset($_GET['add']) || isset($_GET['edit'])) {
$brandquery =$db->query("SELECT * FROM brand");
$parentQuery = $db->query("SELECT * FROM categories WHERE parent = '0' ORDER BY category");
$sizesArray = array();
$title = ((isset($_POST['title']) && $_POST['title'] != '')?sanitize($_POST['title']):'');
$brand = ((isset($_POST['brand']) && !empty($_POST['brand']))?sanitize($_POST['brand']):'');
$parent = ((isset($_POST['parent']) && !empty($_POST['parent']))?sanitize($_POST['parent']):'');
$category = ((isset($_POST['child']) && !empty($_POST['child']))?sanitize($_POST['child']):'');
$price = ((isset($_POST['price']) && $_POST['price'] != '')?sanitize($_POST['price']):'');
$list_price = ((isset($_POST['list_price']) && $_POST['list_price'] != '')?sanitize($_POST['list_price']):'');
$description = ((isset($_POST['description']) && $_POST['description'] != '')?sanitize($_POST['description']):'');
$sizes = ((isset($_POST['sizes']) && $_POST['sizes'] != '')?sanitize($_POST['sizes']):'');
$sizes = rtrim($sizes,',');
$saved_image = '';


if (isset($_GET['edit'])) {
	$edit_id = (int)$_GET['edit'];
	$productresult = $db->query("SELECT * FROM products WHERE id = '$edit_id' ");
	$product = mysqli_fetch_assoc($productresult);

	if (isset($_GET['delete_image'])) {
		
		$imgi = (int)$_GET['imgi'] - 1;
		$images = explode(',', $product['image']);
		$image_url = $_SERVER['DOCUMENT_ROOT'].$images[$imgi];
		//echo $image_url;
		// unset(BASEURL.)
		unlink($image_url);
		unset($images[$imgi]);
		$imageString = implode(',', $images);
		$db->query("UPDATE products SET image = '{$imageString}' WHERE id = '$edit_id' ");
		header('Location:products.php?edit='.$edit_id); 

	}

	$category = ((isset($_POST['child']) && $_POST['child'] != '' )?sanitize($_POST['child']):$product['categories']);

	$title = ((isset($_POST['title']) && $_POST['title'] != '')?sanitize($_POST['title']):$product['title']);
	$brand = ((isset($_POST['brand']) && $_POST['brand'] != '')?sanitize($_POST['brand']):$product['brand']);
	
	$parentQ = $db->query("SELECT * FROM categories WHERE id = '$category' ");
	$parentResults = mysqli_fetch_assoc($parentQ);

	$parent = ((isset($_POST['parent']) && $_POST['parent'] != '')?sanitize($_POST['parent']):$parentResults['parent']);
	$price = ((isset($_POST['price']) && $_POST['price'] != '')?sanitize($_POST['price']):$product['price']);
	$list_price = ((isset($_POST['list_price']) && $_POST['list_price'] != '')?sanitize($_POST['list_price']):$product['list-price']);
	$description = ((isset($_POST['description']) && $_POST['description'] != '')?sanitize($_POST['description']):$product['description']);
	$sizes = ((isset($_POST['sizes']) && $_POST['sizes'] != '')?sanitize($_POST['sizes']):$product['sizes']);
	$sizes = rtrim($sizes,',');
	$saved_image = (($product['image'] != '')?$product['image']:'');
	$dbpath = $saved_image;
	}

			if (!empty($sizes)) {
				$sizeString = sanitize($sizes);
				$sizeString = rtrim($sizeString,',');
				$sizesArray = explode(',', $sizeString);
				$sArray = array();
				$qArray = array();
				$tArray = array();
				foreach ($sizesArray as $ss ) {
					$s =explode(':', $ss);
					$sArray[] = $s[0];
					$qArray[] = $s[1];
					$tArray[] = $s[2];		
				}
			}else{
				$sizesArray =array();
			}

		if ($_POST){
			
			// echo $ptitle.','.$pbrand.','.$pcategories.','.$pprice.','.$plist_price.','.$psizes.','.$pdescription ;
			$errors = array();
			$required = array('title','brand','price','parent','child','sizes');

			$allowed = array('png','jpg','jpeg','gif');
			$uploadPath = array();
			
			foreach ($required as $field) {
				if ($_POST[$field] == '') {
					$errors[] ='All Field with * is required.';
					break;
				}
			}
			//var_dump($_FILES['photo']);
			$photoCount = count($_FILES['photo']['name']);
			//echo $photoCount;
			
			if($photoCount > 0) {
				for($i=0; $i < $photoCount; $i++) { 
					
					$name = $_FILES['photo']['name'][$i];
					//echo $name;
					//echo($name);
					$namearray = explode('.', $name);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = isset($namearray[1]) ? $namearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					$mime = explode('/',$_FILES['photo']['type'][$i]);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$tmpLoc[] = $_FILES['photo']['tmp_name'][$i];
					$fileSize = $_FILES['photo']['size'][$i];
					$uploadName = md5(microtime().$i).'.'.$fileExt;
					$uploadPath[] =  BASEURL.'/UniqueStore/images/products/'.$uploadName;
					
					if ($i != 0 ) {
						$dbpath .= ',';
					}
					$dbpath .= '/UniqueStore/images/products/'.$uploadName;

					if ($mimeType != 'image') {
						$errors[] = 'The file must be an image';
					}
					if (!in_array($fileExt, $allowed)) {
						$errors[] = 'The file extension must be a png, jpg, jgep, or gif';
					}
					if ($fileSize > '8388608') {
						$errors[] = 'The File size must be below 8Mb';
					}

			
				}
			}

			if (!empty($errors)) {
				echo display_errors($errors);
			}else{

				//echo "hello";
				if ($photoCount > 0) {
						
					for ($i=0; $i < $photoCount ; $i++) { 
						//echo $i.',';
						//echo $tmpLoc[$i].',';
						//echo $uploadPath[$i].',';
						move_uploaded_file($tmpLoc[$i], $uploadPath[$i]);
					}
				}

				// // //update file and file 
				// // move_uploaded_file($tmpLoc, $uploadPath);
				$insertSql = "INSERT  INTO products (`title`,`price`,`list-price`,`brand`,`categories`,`image`,`description`,`sizes`) VALUES ('$title','$price','$list_price','$brand','$category','$dbpath','$description','$sizes')";
				
				if (isset($_GET['edit'])) {
					//echo $_GET['edit'];
					// echo $title.','.$price.','.$list_price.','.$brand.','.$category.','.$dbpath.','.$description.','.$sizes.',';
					$insertSql = "UPDATE products SET `title` = '$title', `price` = '$price', `list-price` = '$list_price', `brand` = '$brand', `categories` = '$category', `image` = '$dbpath', `description` = '$description', `sizes` = '$sizes' WHERE id = '$edit_id'"; 
				}
				
				$db->query($insertSql);
				//header("Refresh:0; url=products.php");
				header('Location: products.php');
			 }

		}

?>
<h3 class="text-center"><?=((isset($_GET['edit'])) ?'Edit':'Add a New');?>  Product</h3>
<div class="row">
	<form action="products.php?<?=((isset($_GET['edit']))?'edit='.$edit_id:'add=1');?>" method="POST" enctype="multipart/form-data">
		<div class="form-group col-md-3">
			<label for="title">Title *:</label>
			<input type="text" name="title" class="form-control" id="title" value="<?=$title;?>">
		</div>
		<div class="form-group col-md-3">
			<label for="brand">Brand *:</label>
			<select class="form-control" id="brand" name="brand">
				<option value=""<?=(($brand == '')?' selected':'');?> ></option>
			<?php while($b = mysqli_fetch_assoc($brandquery)): ?>	
				<option value="<?=$b['id']; ?>"<?=(($brand == $b['id'])?' selected':'' );?>> <?=$b['brand'];?></option>
			<?php endwhile; ?>	
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="parent">Parent Category *:</label>
			<select class="form-control" id="parent" name="parent">
				<option value="" <?=(( $parent == '')?' selected':'');?>></option>
				<?php while($p = mysqli_fetch_assoc($parentQuery)): ?>
					<option value="<?=$p['id'];?>" <?=(($parent == $p['id'])?' selected':'');?>><?=$p['category'] ;?></option>
				<?php endwhile; ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="child">Child Category *:</label>
			<select class="form-control" id="child" name="child">
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="price">Price *:</label>
			<input type="text" name="price" id="price" class="form-control" value="<?=$price;?>">
		</div>
		<div class="form-group col-md-3">
			<label for="list_price">List Price *:</label>
			<input type="text" name="list_price" id="list_price" class="form-control" value="<?=$list_price; ?>">
		</div> 
		<div class="form-group col-md-3">
			<label > Quantity & sizes *:</label>
			<button class="btn btn-raised btn-danger" onclick="jQuery('#sizesModal').modal('toggle');return false;">Quantity & sizes</button>
		</div>
		<div class="form-group col-md-3">
			<label for="sizes">Sizes & Qty Preview</label>
			<input type="text" name="sizes" id="sizes" class="form-control" value="<?=$sizes;?>" readonly>
		</div>
		<div class="form-group col-md-6">
			<?php if($saved_image != ''): ?>
				<?php
					$imgi = 1;
					$images = explode(',', $saved_image);
				 ?>
				 <?php foreach($images as $image): ?>
				<div class="saved-image col-md-4">
					<img class="img-responsive" style="height: 150px;" src="<?=$image;?>" ><br> 
					<a href="products.php?delete_image=1&edit=<?=$edit_id;?>&imgi=<?=$imgi;?>" class="btn btn-raised btn-primary">Delete Image</a>
				</div>
				<?php $imgi++;
				endforeach; ?>
			<?php else: ?>
				<label for="photo">Product Photo :</label>
				<input type="file" name="photo[]" id="photo" class="form-control" multiple>
			<?php endif; ?>
		</div>
		<div class="col-md-6 form-group">
			<label for="description">Description</label>
			<textarea id="description" name="description" class="form-control" rows="6"><?=$description;?></textarea>
		</div>
		<div class="form-group col-md-12 text-center">

			<a href="products.php" class="btn btn-raised btn-primary">Cancel</a>
			<input type="submit" value="<?=((isset($_GET['edit'])) ?'Edit':'Add');?> Product" class=" btn btn-raised btn-success">	
		</div><div class="clearfix"></div>
		

		
	</form>

	<!-- modal -->
	<div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="sizesModalLabel">Size & Quantity</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
					<?php for($i=1;$i<=6;$i++): ?>
						<div class="form-group col-md-2">
							<label for="size<?=$i;?>">Size :</label>
							<input type="text" id="size<?=$i;?>" value="<?=((!empty($sArray[$i-1]))?$sArray[$i-1]:'');?>" class="form-control"  name="size<?=$i;?>" >
						</div>
						<div class="form-group col-md-2">
							<label for="size<?=$i;?>">Quantity :</label>
							<input type="number" id="qty<?=$i;?>" name="qty<?=$i;?>" value="<?=((!empty($qArray[$i-1]))?$qArray[$i-1]:'');?>" min="0" class="form-control">
						</div>
						<div class="form-group col-md-2">
							<label for="threshold<?=$i;?>">Threshold :</label>
							<input type="number" id="threshold<?=$i;?>" name="threshold<?=$i;?>" value="<?=((!empty($tArray[$i-1]))?$tArray[$i-1]:'');?>" min="0" class="form-control">
						</div>

					<?php endfor; ?>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-raised btn-default" data-dismiss="modal" >Close</button>
					<button type="button" class="btn btn-raised btn-danger" onclick="updateSizes();jQuery('#sizesModal').modal('toggle');return false;">Save Changes</button>
				</div>
			</div>
		</div>
	</div>



</div>

<?php }else{

$sql = "SELECT * FROM products WHERE deleted = '0'";
$proresult = $db->query($sql);
 
if (isset($_GET['featured'])) {
 	$id = (int)$_GET['id'];
 	$featured = (int)$_GET['featured'];
 	$featuredsql = " UPDATE products SET featured = '$featured' WHERE id = '$id' ";
 	$db->query($featuredsql);
 	header('Location: products.php');
 } 
 if (isset($_GET['cardproduct'])) {
 	$id = (int)$_GET['id'];
 	$cardproduct = (int)$_GET['cardproduct'];
 	$cardproductsql = " UPDATE products SET cardproduct = '$cardproduct' WHERE id = '$id' ";
 	$db->query($cardproductsql);
 	header('Location: products.php');
 } 
 if (isset($_GET['productslider'])) {
 	$id = (int)$_GET['id'];
 	$productslider = (int)$_GET['productslider'];
 	$productslidersql = " UPDATE products SET productslider = '$productslider' WHERE id = '$id' ";
 	$db->query($productslidersql);
 	header('Location: products.php');
 } 
 if (isset($_GET['newproduct'])) {
 	$id = (int)$_GET['id'];
 	$newproduct = (int)$_GET['newproduct'];
 	$newproductsql = " UPDATE products SET newproduct = '$newproduct' WHERE id = '$id' ";
 	$db->query($newproductsql);
 	header('Location: products.php');
 } 
 if (isset($_GET['trendingproduct'])) {
 	$id = (int)$_GET['id'];
 	$trendingproduct = (int)$_GET['trendingproduct'];
 	$trendingproductsql = " UPDATE products SET trendingproduct = '$trendingproduct' WHERE id = '$id' ";
 	$db->query($trendingproductsql);
 	header('Location: products.php');
 } 
 if (isset($_GET['saleproduct'])) {
 	$id = (int)$_GET['id'];
 	$saleproduct = (int)$_GET['saleproduct'];
 	$saleproductsql = " UPDATE products SET saleproduct = '$saleproduct' WHERE id = '$id' ";
 	$db->query($saleproductsql);
 	header('Location: products.php');
 } 
  if (isset($_GET['tabproductupper'])) {
 	$id = (int)$_GET['id'];
 	$tabproductupper = (int)$_GET['tabproductupper'];
 	$tabproductuppersql = " UPDATE products SET tabproductupper = '$tabproductupper' WHERE id = '$id' ";
 	$db->query($tabproductuppersql);
 	header('Location: products.php');
 } 
   if (isset($_GET['tabproductlower'])) {
 	$id = (int)$_GET['id'];
 	$tabproductlower = (int)$_GET['tabproductlower'];
 	$tabproductlowersql = " UPDATE products SET tabproductlower = '$tabproductlower' WHERE id = '$id' ";
 	$db->query($tabproductlowersql);
 	header('Location: products.php');
 } 
 
?>
<div class="text-center">
	<a href="products.php?add=1" class="btn btn-raised btn-success" id="add-product-btn">Add a Product</a>
</div>
<br>
<div style="overflow-x:auto; overflow-y: auto;">
<table class="table table-striped">
	<thead>
		<th>Edit-Remove</th>
		<th>Product</th>
		<th>Price</th>
		<th>Category</th>
		<th>Image</th>
		<th>Featured</th>
		<th>Tab items upper</th>
		<th>Tab items lower</th>
		<th>Card Products</th>
		<th>Main Slider Product</th>
		<th>New Products</th>
		<th>Trending Product</th>
		<th>Sale Product</th>
		<th>Sold</th>
	</thead>
	<tbody>
	<?php while($product = mysqli_fetch_assoc($proresult)):

	$childID = $product['categories'];
	$catsql  = "SELECT * FROM categories Where id = $childID";
	$result = $db->query($catsql);
	$child = mysqli_fetch_assoc($result);
	$parentID = $child['parent'];
	$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
	$presult = $db->query($psql);
	$parent = mysqli_fetch_assoc($presult);
	$category = $parent['category'].'-'.$child['category'];
	
	?>
		<tr>
			<td>
				<a href="products.php?edit=<?=$product['id']; ?>" class="btn btn-raised btn-xs btn-danger"><span class="glyphicon glyphicon-pencil"></span></a>				
				<a href="products.php?delete=<?=$product['id']; ?>" class="btn btn-raised btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>

			</td>
			<td><h5><?=$product['title']; ?></h5></td>
			<td><h5><?=money($product['price']);?></h5></td>
			<td><h5><?=$category; ?></h5></td>
			<?php $photos = explode(',',$product['image']);?>
			<td><img src="<?=$photos[0];?>" alt="product image" height = "80px" width = "80px" ></td>			
			<td><a href="products.php?featured=<?=(($product['featured']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['featured']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['featured']=='1')?'Featured Product':''); ?>
			</td>
			<td><a href="products.php?tabproductupper=<?=(($product['tabproductupper']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['tabproductupper']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['tabproductupper']=='1')?'Tab Product':''); ?>
			</td>
			<td><a href="products.php?tabproductlower=<?=(($product['tabproductlower']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['tabproductlower']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['tabproductlower']=='1')?'Tab Product':''); ?>
			</td>
			<td><a href="products.php?cardproduct=<?=(($product['cardproduct'] == '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['cardproduct']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['cardproduct']=='1')?'Card Product':''); ?>
			</td>
			<td><a href="products.php?productslider=<?=(($product['productslider']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['productslider']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['productslider']=='1')?'Product Slider':''); ?>
			</td>
			<td><a href="products.php?newproduct=<?=(($product['newproduct']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['newproduct']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['newproduct']=='1')?'New Product':''); ?>
			</td>
			<td><a href="products.php?trendingproduct=<?=(($product['trendingproduct']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['trendingproduct']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['trendingproduct']=='1')?'Trending Product':''); ?>
			</td>
			<td><a href="products.php?saleproduct=<?=(($product['saleproduct']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-raised btn-danger"><span class="glyphicon glyphicon-<?=(($product['saleproduct']== '1')?'minus':'plus'); ?>"></span> </a>&nbsp <?=(($product['saleproduct']=='1')?'Sale Product':''); ?>
			</td>
			<td><h5><?=$product['deleted'];?></h5></td>
		</tr>
	<?php endwhile; ?>
	</tbody>		
</table>
</div>


<?php } include'includes/footer.php';
 ?>

<script type="text/javascript">
	jQuery('document').ready(function(){
		get_child_options('<?=$category;?>');
	});
</script>

 </body>
 </html>
