<footer class="text-center col-md-12 footer" id="footer">&copy; Copyright 2013-2017</footer>
<script type="text/javascript">

function updateSizes(){
	var sizeString = '';
	for (var i = 1 ; i <= 6; i++) {
	 	if (jQuery('#size'+i).val()!= '') {
	 		sizeString += jQuery('#size'+i).val()+':'+jQuery('#qty'+i).val()+':'+jQuery('#threshold'+i).val()+',';
	 	}
	 } 
	 jQuery('#sizes').val(sizeString);
}

function get_child_options(selected) {

		if (typeof selected == 'undefined') {
			var selected = '';
		}
		var parentID = jQuery("#parent").val();
		jQuery.ajax({
			url: '/UniqueStore/adminnew/parsers/child_categories.php',
			type: 'POST',
			data: {parentID : parentID, selected: selected},
			success: function(data){
				jQuery('#child').html(data);
			},
			error: function(){
				alert("something went wrong with child options.")
			},
		});
}	
jQuery('select[name="parent"]').change(function(){
	get_child_options(); 
});

</script>
