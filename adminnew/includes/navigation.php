<!--Top navbar-->
	
<div class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/UniqueStore/adminnew/index.php">UniqueStore Admin</a>
    </div>
    <div class="navbar-collapse collapse navbar-responsive-collapse">
      <ul class="nav navbar-nav">
        <li><a href="index.php">My Dashboard</a></li>
       	<li><a href="brand.php">Brands</a></li>
		<li><a href="categories.php">Categories</a></li>
		<li><a href="products.php">Products</a></li>
		<li><a href="archived.php">Archived</a></li>
		<?php  $result = has_permission(); 
		if( $result == '1'): ?>
		<li><a href="users.php">Users</a></li>
	<?php endif;?>
       
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
		    	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello <?=$_SESSION['first']?>!
		    		<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
		    	</a>
		    	<ul class="dropdown-menu" role="menu">
		    		<li><a href="change_password.php">Change Password</a></li>
		    		<li><a href="logout.php">Logout</a></li>
		    	</ul>
		</li>
      </ul>
    </div>
  </div>
</div>
<div class="container-fluid">