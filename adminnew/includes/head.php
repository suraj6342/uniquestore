<!DOCTYPE html>
<html>
<head>
	<title>Administrator</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-material-design.min.css">  

<script src="../js/jquery.js"></script>
<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/material.min.js"></script>
<link href="css/style.css" rel="stylesheet">

</head>
<body>
