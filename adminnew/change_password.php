 <?php

require_once $_SERVER['DOCUMENT_ROOT'].'/UniqueStore/core/db.php';

$result = is_logged_in();

//echo $result;
if ($result == '0' ) {
	login_error_redirect();
}

include'includes/head.php';

$hashed = $user_data['password'];
//echo $hashed;
$old_password = ((isset($_POST['old_password']))?sanitize($_POST['old_password']):'');
$old_password = trim($old_password);

$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
$password = trim($password);
$confirm = ((isset($_POST['confirm']))?sanitize($_POST['confirm']):'');
$confirm = trim($confirm);
$new_hashed = password_hash($password, PASSWORD_DEFAULT);
$user_id = $user_data['id'];
$errors = array();


?>

<style type="text/css">
body{
	background-color: lightgrey;
}
</style>

<div>
	<?php 
			if ($_POST) {
				if(empty($_POST['old_password']) || empty($_POST['password']) || empty($_POST['confirm']) ){
					$errors[] = 'You must fill out all fields.';
				}

				//password is more than 6 charackter
				if (strlen($password) < 6 ) {
					$errors[] = 'Password must be atleast 6 character';
				}

				//if new password matches confirm

				if($password != $confirm){
					$errors[] = 'The new password and confirm password does not match';
				}

				if(!password_verify($old_password, $hashed)){
					$errors[] = 'Your old password does not match our records.';
				}

				//check for errors
				if(!empty($errors)){
					echo display_errors($errors);
				}else{
					//change ppassword
					$db->query("UPDATE users SET password = '$new_hashed' WHERE id='$user_id' ");
					$_SESSION['success_flash'] = 'Your password has been updated';
					header('Location:index.php');

				}
			}
		?>
</div>


<div id="login-form">
	<h3 class="text-center">Change Password</h3><hr>
	<form action="change_password.php" method="post">	
		<div class="form-group">
			<label for="old_password">Old Password :</label>
			<input type="password" name="old_password" id="old_password" class="form-control" value="<?=$old_password;?>">
		</div>
		<div class="form-group">
			<label for="email">New Password :</label>
			<input type="password" name="password" id="password" class="form-control" value="<?=$password;?>">
		</div>
		<div class="form-group">
			<label for="confirm">Confirm Password :</label>
			<input type="password" name="confirm" id="confirm" class="form-control" value="<?=$confirm;?>">
		</div>	
		<div class="form-group">
			<a href="index.php" class="btn btn-default form-group">Cancel</a>
			<input type="submit" class="form-group btn btn-danger">
		</div>
		<hr>
	</form>
	
</div>


<?php  include'includes/footer.php';
 ?>


 </body>
 </html>
