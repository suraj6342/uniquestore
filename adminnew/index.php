<?php 
require_once'../core/db.php';
$result = is_logged_in();
//echo $result;
if ($result == '0' ) {
	login_error_redirect();
}
// $result1 = has_permission();
// echo $result1;
// if( $result1 == '1') {
// 	//echo "has permission";  	
//  }elseif ($result1 == '0') {
//  	permission_error_redirect();
//  }
include'includes/head.php';
include'includes/navigation.php';
?>

<!-- Orders to Fill -->
<?php 
	$txnQuery = "SELECT t.id, t.cart_id, t.full_name, t.description, t.txn_date, t.grand_total, c.items, c.paid, c.shipped
		FROM transactions t
		LEFT JOIN cart c ON t.cart_id = c.id
		WHERE c.paid = 1 AND c.shipped = 0
		ORDER BY t.txn_date ";
		$txnResults = $db->query($txnQuery);

?>
<?php 
	$txnQuery1 = "SELECT t.id, t.cart_id, t.full_name, t.description, t.txn_date, t.grand_total, c.items, c.paid, c.shipped
		FROM transactions t
		LEFT JOIN cart c ON t.cart_id = c.id
		WHERE c.paid = 1 AND c.shipped = 1
		ORDER BY t.txn_date ";
		$txnResults1 = $db->query($txnQuery1);

?>
<div class="col-md-12">
	<h3 class="text-center">Orders To Ship</h3>
	<div style="overflow-x:auto; overflow-y: auto;">
	<table class="table table-striped">
		<thead>
			<th></th>
			<th>Name</th>
			<th>Description</th>
			<th>Total</th>
			<th>Date</th>
		</thead>
		<tbody>
			<?php while($order = mysqli_fetch_assoc($txnResults)): ?>
			<tr>
				<td><a href="order.php?txn_id=<?=$order['id'];?>" class="btn btn-xs btn-raised btn-info">Details</a></td>
				<td><?=$order['full_name'];?></td>
				<td><?=$order['description'];?></td>
				<td><?=money($order['grand_total']);?></td>
				<td><?=pretty_date($order['txn_date']) ;?></td>
			</tr>
			<?php endwhile; ?>
		</tbody>
	</table>
	</div>
</div>
<div class="col-md-12">
	<h3 class="text-center">Shipped order</h3>
	<div style="overflow-x:auto; overflow-y: auto;">
	<table class="table table-striped">
		<thead>
			<th></th>
			<th>Name</th>
			<th>Description</th>
			<th>Total</th>
			<th>Date</th>
		</thead>
		<tbody>
			<?php while($shipped_order = mysqli_fetch_assoc($txnResults1)): ?>
			<tr>
				<td><a href="shippedorder.php?txn_id=<?=$shipped_order['id'];?>" class="btn btn-xs btn-raised btn-info">Details</a></td>
				<td><?=$shipped_order['full_name'];?></td>
				<td><?=$shipped_order['description'];?></td>
				<td><?=money($shipped_order['grand_total']);?></td>
				<td><?=pretty_date($shipped_order['txn_date']) ;?></td>
			</tr>
			<?php endwhile; ?>
		</tbody>
	</table>
	</div>
</div>
<div class="col-md-4 mt-15">
		<!-- sales by month -->
			<?php 
			$thisYr = date("Y");
			$lastYr = $thisYr - 1;
			// echo $thisYr;
			// echo $lastYr;
			$thisYrQ = $db->query("SELECT grand_total, txn_date FROM transactions WHERE YEAR(txn_date) = '{$thisYr}'");
			$lastYrQ = $db->query("SELECT grand_total, txn_date FROM transactions WHERE YEAR(txn_date) = '{$lastYr}'");
			$current = array();
			$last = array();
			$currentTotal = 0;
			$lastTotal = 0;
			while ($x = mysqli_fetch_assoc($thisYrQ)) {
				$month = date("m",strtotime($x['txn_date']));
				if (!array_key_exists($month, $current)) {
					$current[(int)$month] = $x['grand_total'];
				}else{
					$current[(int)$month] += $x['grand_total'];
				}
				$currentTotal += $x['grand_total'];
			}
			while ($y = mysqli_fetch_assoc($lastYrQ)) {
				$month = date("m",strtotime($y['txn_date']));
				if (!array_key_exists($month, $current)) {
					$last[(int)$month] = $y['grand_total'];
				}else{
					$last[(int)$month] += $y['grand_total'];
				}
				$lastTotal += $y['grand_total'];

			}
			//echo $currentTotal;

			echo  $x['grand_total'];
		?>
			<h3 class="text-center">Sales By Month</h3>
			<table class="table table-striped">
				<thead>
					<th>Month</th>
					<th><?=$lastYr;?></th>
					<th><?=$thisYr;?></th>
				</thead>
				<tbody>
					<?php for($i=1; $i <= 12; $i++):
						$dt = DateTime::createFromFormat('!m',$i);
					 ?>
					<tr<?=(date("m") == $i)?' class="info-btn"':'';?>>
						<td><?=$dt->format("F");?></td>
						<td><?=(array_key_exists($i, $last))?money($last[$i]):money(0);?></td>
						<td><?=(array_key_exists($i, $current))?money($current[$i]):money(0);?></td>
					</tr>
					<?php endfor; ?>
					<tr>
						<th>Total</th>
						<th><?=money($lastTotal);?></th>
						<th><?=money($currentTotal);?></th>
					</tr>
				</tbody>
			</table>
		</div>
<!-- Inventory -->

<?php  
	$iQuery = $db->query("SELECT * FROM products WHERE deleted = 0");
	$lowItems = array();
	while($product = mysqli_fetch_assoc($iQuery)) {
		$item = array();
		$sizes = sizesToArray($product['sizes']);
		foreach($sizes as $size) {
			if ($size['quantity'] <= $size['threshold']) {
				$cat = get_Category($product['categories']);
				$item = array(
				'title' => $product['title'],
				'image' => $product['image'],
				'size' => $size['size'],
				'quantity' => $size['quantity'],
				'threshold' => $size['threshold'],
				'category' => $cat['parent'].'~'.$cat['child']
				);
				$lowItems[] =$item;
			}
		}
	}
?>

<div class="col-md-8 mt-15">
	<h3 class="text-center">Low Inventory</h3>
	<div style="overflow-x:auto; overflow-y: auto;">
	<table class="table table-striped">
		<thead>
			<th>Product</th>
			<th>Image</th>
			<th>Category</th>
			<th>Size</th>
			<th>Quantity</th>
			<th>Threshold</th>
		</thead>
		<tbody>
		<?php foreach($lowItems as $item): ?>
			<tr <?=($item['quantity'] == 0)?' class="danger"':'';?>>
				<td><?=$item['title'];?></td>
				<?php $photos = explode(',',$item['image']);?>
				<td><img src="<?=$photos[0];?>" alt="product image" height = "80px" width = "80px" ></td>
				<td><?=$item['category'];?></td>
				<td><?=$item['size'];?></td>
				<td><?=$item['quantity'];?></td>
				<td><?=$item['threshold'];?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	</div>
</div>
</div>
	
<?php include'includes/footer.php';
?>

 </body>
 </html>