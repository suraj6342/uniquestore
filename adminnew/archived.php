<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/UniqueStore/core/db.php';

$result = is_logged_in();

//echo $result;
if ($result == '0' ) {
	login_error_redirect();
}
include'includes/head.php';
include'includes/navigation.php';

$sql = "SELECT * FROM products WHERE deleted = '1' ";
$arcresult = $db->query($sql);

//restore products
if (isset($_GET['restore'])) {
	$restoreid = sanitize($_GET['restore']);
	$db->query("UPDATE products SET deleted = 0 WHERE id = '$restoreid'");
	header('Location: archived.php');
}

?>
<h3 class="text-center"> Archieve Products </h3>
<br>
<div class="container-fluid">
	<div class="row">
	<div style="overflow-x:auto; overflow-y: auto;">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Restore</th>
					<th>Product</th>
					<th>Price</th>
					<th>Brand</th>
					<th>Image</th>
					<th>Category</th>
					<th>Sold</th>
				</tr>
			</thead>
			<tbody>
			<?php while($arc = mysqli_fetch_assoc($arcresult)): 
			$bid = $arc['brand'];
			$bsql = "SELECT * FROM brand WHERE id = $bid ";
			$bresult = $db->query($bsql);
			$b = mysqli_fetch_assoc($bresult);
			$catid = $arc['categories'];
			$catsql = "SELECT * FROM categories WHERE id = $catid ";
			$catresult = $db->query($catsql);
			$cat = mysqli_fetch_assoc($catresult);
			$catparid = $cat['parent'];
			$catparsql = "SELECT * FROM categories Where id = $catparid";
			$catpatresult = $db->query($catparsql);
			$parent = mysqli_fetch_assoc($catpatresult);

			?>
				<tr>
					<td><a href="archived.php?restore=<?=$arc['id'];?>" class="btn btn-xs btn-raised btn-primary "><span class="glyphicon glyphicon-refresh"></span></a></td>
					<td><?=$arc['title'];?></td>
					<td><?=$arc['price'];?></td>
					<td><?=$b['brand'];?></td>
					<?php $photos = explode(',',$arc['image']);?>
					<td><img src="<?=$photos[0];?>" alt="product image" height = "80px" width = "80px" ></td>
					<td><?=$parent['category'];?> - <?=$cat['category'];?></td>
					<td><?=$arc['deleted'];?></td>
				</tr>
			<?php endwhile; ?>
			</tbody>
		</table>
	</div>
	</div>
</div>

<?php  include'includes/footer.php';
 ?>


 </body>
 </html>
