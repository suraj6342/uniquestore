<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/UniqueStore/core/db.php';

$result = is_logged_in();
//echo $result;
if ($result == '0' ) {
	login_error_redirect();
}
$result1 = has_permission();
//echo $result1;
if( $result1 == '0') {
	//echo "has permission";  	
 //}elseif ($result1 == '0') {
 	permission_error_redirect();
}

include'includes/head.php';
include'includes/navigation.php';

if(isset($_GET['delete'])){
	$delete_id = sanitize($_GET['delete']);
	$db->query("DELETE FROM users WHERE id = '$delete_id' ");
	$_SESSION['success_flash'] = 'User has been deleted!';
	header('Location:users.php');
}

if (isset($_GET['add'])) {

	$name = ((isset($_POST['name']))?sanitize($_POST['name']):'');
	$email = ((isset($_POST['email']))?sanitize($_POST['email']):'');
	$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
	$confirm = ((isset($_POST['confirm']))?sanitize($_POST['confirm']):'');
	$permissions = ((isset($_POST['permissions']))?sanitize($_POST['permissions']):'');
	//echo $permissions;
	//echo $name;
	$errors = array();
	

	if ($_POST) {


		$emailQuery = $db->query("SELECT * FROM users WHERE email = '$email'");
		$emailCount = mysqli_num_rows($emailQuery);

		if ($emailCount != 0 ) {
			$errors[] = 'That email already exist in our database';
		}

		if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['confirm']) || empty($_POST['permissions'])  ) {
				
			$errors[] = 'You must fill out all fields';
		}	
		

		if (strlen($password) < 6 ) {
			$errors[] = 'Password must be at least 6 character';
		}

		if ($password != $confirm) {
			$errors[] = 'Your Password and confirm password is not match.';
		}


		if (!empty($errors)) {
			echo display_errors($errors);
		}else{

			//add user to db
			$hashed = password_hash($password, PASSWORD_DEFAULT);
			$db->query("INSERT INTO users (full_name,email,password,permissions) VALUES ('$name','$email','$hashed','$permissions')");
			$_SESSION['success_flash'] = 'User has been added!';
			header('Location:users.php');
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors[] = 'You must enter a valid email id';
		}
	}

?>

<h3 class="text-center">Add a new User</h3>
<form action="users.php?add=1" method="post"> 
<div class="form-group col-md-6">
	<label for="name">Full Name:</label>
	<input type="text" name="name" id="name" class="form-control" value="<?=$name;?>">
</div>
<div class="form-group col-md-6">
	<label for="email">Email:</label>
	<input type="text" name="email" id="email" class="form-control" value="<?=$email;?>">
</div>
<div class="form-group col-md-6">
	<label for="password">Password :</label>
	<input type="password" name="password" id="password" class="form-control" value="<?=$password;?>">
</div>
<div class="form-group col-md-6">
	<label for="confirm">Confirm Password :</label>
	<input type="password" name="confirm" id="confirm" class="form-control" value="<?=$confirm;?>">
</div>
<div class="form-group col-md-6">
	<label for="permissions">Permissions :</label>
	<select class="form-control" name="permissions" id="permissions">
		<option value=""<?=(($permissions == '')?' selected':'');?>></option>
		<option value="editor"<?=(($permissions == 'editor')?' selected':'');?>> Editor </option>
		<option value="admin,editor"<?=(($permissions == 'admin,editor')?' selected':'');?>> Admin </option>
	</select>
</div>
<div class="form-group col-md-6 controls">
	<a href="users.php" class="btn btn-raised btn-default">Cancel</a>
	<input type="submit" value="Add User" class="btn btn-raised btn-danger">
</div>

</form>
<?php
	
}else{
$userQuery = $db->query("SELECT * FROM users ORDER BY full_name");

?>
<div class="text-center">
	<a href="users.php?add=1" class="btn btn-raised btn-success" >Add New User</a>
</div>
<br><br>
<table class="table table-striped">
	<thead>
		<th></th>
		<th>Name</th>
		<th>Email</th>
		<th>Join Date</th>
		<th>Last Login</th>
		<th>Permissions</th>
	</thead>
	<tbody>
	<?php while($user = mysqli_fetch_assoc($userQuery)): ?>

		<tr>
			<td>
				<?php if($user['id'] != $user_data['id']) : ?>
					<a href="users.php?delete=<?=$user['id'];?>" class= "btn btn-raised btn-xs btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a>
				<?php endif; ?>
			</td>
			<td><h5><?=$user['full_name'];?></h5></td>
			<td><h5><?=$user['email'];?></h5></td>
			<td><h5><?=pretty_date($user['join_data']);?></td>
			<td><h5><?=(($user['last_login'] == '0000-00-00 00:00:00')?'Never':pretty_date($user['last_login']));?></h5></td>
			<td><h5><?=$user['permissions'];?></h5></td>
		</tr>
	<?php endwhile; ?>
	</tbody>
</table>

<?php } include'includes/footer.php';
 ?>

 </body>
 </html>