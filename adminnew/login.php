<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/UniqueStore/core/db.php';
include'includes/head.php';

$email = ((isset($_POST['email']))?sanitize($_POST['email']):'');
$email = trim($email);
$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
$password = trim($password);

$errors = array();
?>


	<?php 
			if ($_POST) {
				if(empty($_POST['email']) || empty($_POST['password'])){
					$errors[] = 'You must provide email and password.';
				}

				//VALIDATE EMAIL

				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$errors[] = 'You must enter a valid email address'; 
				}

				//password is more than 6 charackter
				if (strlen($password) < 6 ) {
					$errors[] = 'Password must be atleast 6 character';
				}

				//check if user exist in database

				$query = $db->query("SELECT * FROM users WHERE email = '$email'");
				$user = mysqli_fetch_assoc($query);
				$usercount = mysqli_num_rows($query);
				//echo $usercount;
				if ($usercount < 1){

					$errors[] = 'That email does not exist in our database'; 
				}

				// if($password != $user['password']){
				// 	$errors[] = 'The password does not match our records. Please try again';
				// }



				//check for errors
				if(!empty($errors)){
					echo display_errors($errors);
				}else{
					//user login 
					//echo "log user in";
					 $user_id = $user['id'];
						//echo $user_id;
					 login($user_id);

				}
			}
		?>
<!--Top navbar-->
	
<div class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      
      <a class="navbar-brand" href="/UniqueStore/adminnew/index.php">UniqueStore Admin</a>
    </div>
  </div>
</div>
<div class="container-fluid">
	<div class="row">
		<div style="background-color: gray;" class="col-xs-10 col-sm-6 col-md-6 col-xs-offset-1 col-sm-offset-3 col-md-offset-3 login-form text-center">
				<h3 class="text-center">Login</h3><hr style="width: 40%;">
				<form action="login.php" method="post">	
					<div class="form-group">
						<label for="email">Email :</label>
						<input type="text" name="email" id="email" class="form-control" value="<?=$email;?>">
					</div>
					<div class="form-group">
						<label for="email">Password :</label>
						<input type="password" name="password" id="password" class="form-control" value="<?=$password;?>">
					</div>	
					<div class="form-group">
						<input type="submit" class="btn btn-raised btn-danger" value="Login">
					</div>
				</form>
				<p class="text-left"><a href="/UniqueStore/index.php" class="btn btn-raised btn-primary" alt="home">Visit Site</a></p>
				<p class="text-right">For Demo Email: admin@gmail.com Pass:123456</p>
		</div>
	</div>
</div>

<?php  include'includes/footer.php';
 ?>


 </body>
 </html>
