<?php 
require_once'../core/db.php';

$result = is_logged_in();

//echo $result;
if ($result == '0' ) {
	login_error_redirect();
}

include'includes/head.php';
include'includes/navigation.php';
$sql = "SELECT * from brand ORDER BY brand";
$results = $db->query($sql);
$errors = array();


//edit brand
if( isset($_GET['edit']) && !empty($_GET['edit'])){
	$edit_id = (int)$_GET['edit'];
	$edit_id = sanitize($edit_id);
	$edit = "SELECT * FROM brand WHERE id ='$edit_id' ";
	$edit_result =$db->query($edit);
	$ebrand = mysqli_fetch_assoc($edit_result);
	
	
}

//delete brand
if( isset($_GET['delete']) && !empty($_GET['delete']) )
{
	$delete_id = (int)$_GET['delete'];
	$delete_id = sanitize($delete_id);
	$delete = "DELETE FROM brand WHERE id = '$delete_id' ";
	$db->query($delete);
	header('Location: brand.php');
}

//if form submitted//

 if (isset($_POST['add_submit'])) {
 	
	 //php method
	 $brand = sanitize($_POST['brand']);
	 //check brand is blank
	 if ($_POST['brand'] =='') {
		$errors[] .='You must enter a brand';
	 	# code...
	 }
	 //check if brand exists in database
	 $sql1 = "SELECT * FROM brand WHERE brand = '$brand' ";
	 if (isset($_GET['edit'])) {
	 	$sql3 = "SELECT * FROM brand WHERE brand = '$brand' AND id != '$edit_id' ";
	 }


	 $results1 = $db->query($sql1);
	 $count = mysqli_num_rows($results1);
	 if($count > 0)
	 {
	 	$errors[] .= 'That Band Allready Exists Please choose another brand';
	 }
	
	 //display errors
	 if (!empty($errors)) {
	 	# code...
	 	echo display_errors($errors);
	 }else{
	 	//add brand to database
	 	$sql2 = "INSERT INTO brand (`brand`) VALUES ('$brand') ";
	 	if (isset($_GET['edit'])) {
	 		$sql2 = "UPDATE brand SET `brand` = '$brand' WHERE id = '$edit_id' ";
	 	}

	 	$db->query($sql2);
	 	$insert_mes = "Brand Inserted";
	 	echo display_errors($insert_mes);
	 	header('Location: brand.php');
	 }
}
?>

<!-- <div class="error-modal text-center col-lg-6 col-lg-offset-3">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="close_modal1"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
	 		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="close_modal2">Close</button>
			</div>
 		</div>
	</div>
</div> -->

				
<!-- brand form -->
<div class="text-center">
	<form class="form-inline" action="brand.php<?=((isset($_GET['edit']))?'?edit='.$edit_id:''); ?>" method="post" enctype="multipart/form-data">
		<div class="form-group brand-form ">

		<!-- $brand_value -->
		<?php 

		$brand_value = '';
		if (isset($_GET['edit'])) {
			$brand_value = $ebrand['brand'];
		}else{

			if (isset($_POST['brand'])) {

				$brand_value = sanitize($_POST['brand']);
			}
		}

		?>


			<label for="brand"><?=((isset($_GET['edit']))?'Edit':'Add a'); ?> Brand:</label>
			<input type="text" name="brand" id="brand-input" class="form-control" value="<?=$brand_value; ?>" autofocus="on">	
			<?php  if(isset($_GET['edit'])) : ?>
				<a href="brand.php" class="btn btn-raised btn-default">Cancel</a>

		<?php endif; ?>
			<input type="submit" name="add_submit" id="add-brand-btn" class="btn btn-raised btn-success" value="<?=((isset($_GET['edit']))?'Edit':'Add a'); ?> Brand"> 
		</div>
	</form>
</div>
<br>
<br>

<table class="table table-striped text-center">
	<thead>
		<th class="text-center"><h4 class="margin-tb-5">Edit</h4></th><th class="text-center"><h4 class="margin-tb-5">Brand</h4></th><th class="text-center"><h4 class="margin-tb-5">Remove</h4></th>
	</thead>
	<tbody>
	<?php while($brand = mysqli_fetch_assoc($results)):?>
		<tr>
		
			<td><a href="brand.php?edit=<?=$brand['id'];?>" class="btn btn-raised  btn-xs btn-primary edit-brand-btn"><span class="glyphicon glyphicon-pencil"></span></a></td>
			<td><h4><?=$brand['brand']; ?></h4></td>
			<td><a href="brand.php?delete=<?=$brand['id'];?>" class="btn btn-raised btn-xs  btn-primary"><span class="glyphicon glyphicon-remove-sign"></span></a></td>
		</tr>
	<?php endwhile; ?>

	</tbody>	
</table>


				




<?php include'includes/footer.php';
 ?>

 <script type="text/javascript">
 	$(document).ready(function(){

	$(".err-modal-btn").click(function(){
			$("#somedialog").removeClass("dialog--open"); 		
	 	});
	$(".err-modal-btn1").click(function(){
			$(".brand-exist-modal").removeClass("dialog--open"); 		
	 	});
	$("#insert-modal-btn").click(function(){

		location.reload();
	});

	$('#close_modal2').click(function(){
		$(".error-modal").addClass("hidden");
	});



});

 </script>
<script type="text/javascript">

	function editinputshow(num){
		
		$(".brand-"+num).toggle();
		$(".edit-input-"+num).toggle();
	}

</script>

 </body>
 </html>

