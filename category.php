<?php 
require'core/db.php';
include'includes/top-header.php';  


$childcatid = $_REQUEST['childcat'];

$categorysql = "SELECT * FROM categories WHERE id = '$childcatid'";
$categoryquery = $db->query($categorysql);
$categoryname = mysqli_fetch_assoc($categoryquery);

$childcatsql = "SELECT * FROM products WHERE categories = '$childcatid'";
$childcatquery = $db->query($childcatsql);


?>
	<section class="page-header col-md-12">
	<div class="container-fuild">
		<div class="row">
			<div class="col-xs-12 col-md-12 no-padding-lf">
				<div class="col-xs-12 col-md-12 backheader">
					
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 frontheader category-frontheader">
					<div class="col-xs-12 col-md-6">
						<h1 style="color: #ffffff;"><?=$categoryname['category'];?></h1>
						<br>
					</div>
					<div class="col-xs-12 col-md-6 frontheader-image text-center">
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>

	<div class="container-fluid product-section">
		<div class="row">
			<div class="col-sm-12 col-md-10 col-md-offset-1 padding-t-40 no-padding-lf pad-lf-15">
				<div class="col-sm-4 col-md-3 left-pad-0">
					<?php include'includes/widgets/filters.php'; ?>
					<div class="hidden-xs" style="background-color: skyblue; height: 300px;" class="banner margin-tb-20">
						
					</div>

				</div>
				<div class="col-sm-8 col-md-9 no-padding-lf padding-lf-15">
					<div class="col-sm-12 col-md-12 padding-tb-30">
						<?php while($product = mysqli_fetch_assoc($childcatquery)): 
						 $childID = $product['categories'];
							$catsql  = "SELECT * FROM categories Where id = $childID";
							$result = $db->query($catsql);
							$child = mysqli_fetch_assoc($result);
							$parentID = $child['parent'];
							$psql = "SELECT * FROM categories WHERE id = '$parentID' ";
							$presult = $db->query($psql);
							$parent = mysqli_fetch_assoc($presult);
							$category = $parent['category'].'-'.$child['category'];

						?>
						<div class="col-sm-6 col-md-4 wow slideInRight">
							<div class="product-inner">
								<a href="single-product.php?productid=<?=$product['id'];?>"><img src="<?=$product['image'];?>" class="img-responsive"></a>
								<h4 class="text-center padding-tb-5"><?=$product['title'];?></h4>
								<h5 class="text-center"><?=$category;?></h5>
								<h5 class="text-center"><?=money($product['price']);?></h5>
								<h5 class="text-center"><s><?=money($product['list-price']);?></s></h5>
								
							</div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php 
include'includes/footer.php';  
?>