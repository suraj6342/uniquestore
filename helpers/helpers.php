
<?php 

function sanitize($dirty){
	return htmlentities($dirty);
}

function display_tab($errors){

	$display =      '<h4 class="modal-body">';
					foreach ($errors as $error) {
							$display .= '<p class="bg-danger error-tab">'.$error.'</p>';
						}
	$display .=      '</h4>';
	return $display;

}
function display_errors($errors){
	
	$display   = 	'<div class="container">';
	$display  .=		'<div class="row">';
	$display  .=      		'<h4 class="modal-body">';
								foreach ($errors as $error) {
										$display .= '<p class="error-tab">'.$error.'</p>';
									}
	$display .=      		'</h4>';
	$display .=      	'</div>';
	$display .=      '</div>';
	
	return $display;
}

function login($user_id){
	 $_SESSION['SBUser'] = $user_id; 
	global $db; 
	$date = date("y-m-d H:i:s");
	$db->query("UPDATE users SET last_login = '$date' WHERE id = '$user_id' ");
	 $_SESSION['success_flash'] = 'You are now logged in';
	header('Location: index.php');
}

function is_logged_in(){
	if (isset($_SESSION['SBUser']) && $_SESSION['SBUser'] > 0) {
		return 1;
	}
	return 0;
 }



function login_error_redirect(){
	$_SESSION['error_flash'] = 'You must be logged in to access that page';
	header('Location: login.php');
}

function permission_error_redirect(){
	$_SESSION['error_flash'] = 'You do not have permission to access that page';
	header('Location: index.php');
}


function has_permission($permission = 'admin'){

	if (isset($_SESSION['SBUser'])) {
		$user_id = $_SESSION['SBUser'];
		global $db;
		$query = $db->query("SELECT * FROM users WHERE id = '$user_id'");
		$user_data = mysqli_fetch_assoc($query);
	
		$permissions = array();
		//echo $user_data['permissions'];
		$permissions = explode(',',$user_data['permissions']);
		if(in_array($permission, $permissions, true)){
			return 1;
		}
		return 0;	
	}
}

function pretty_date($date){
	return date("M d, Y h:i A",strtotime($date));
}

//money_format//

function money($num){
	return '₹'.number_format($num,2);
}


function get_Category($child_id){
	global $db;
	$id = sanitize($child_id);
	$sql ="SELECT p.id AS 'pid', p.category AS 'parent', c.id AS 'cid', c.category AS 'child' 
	FROM categories c 
	INNER JOIN categories p
	ON c.parent = p.id
	WHERE c.id = '$id' ";
	$query = $db->query($sql);
	$category = mysqli_fetch_assoc($query);
	return $category;
}


function sizesToArray($string){

	$sizeArray = explode(',', $string);
	$returnArray = array();
	foreach ($sizeArray as $size) {
		$s = explode(':', $size);
		$returnArray[] = array('size' => $s[0], 'quantity' => $s[1],'threshold' => $s[2]);
	}
	return $returnArray;
}

function sizesToString($sizes){
	$sizeString = '';
	foreach ($sizes as $size) {
		$sizeString .= $size['size'].':'.$size['quantity'].':'.$size['threshold'].',';
	}
	$trimmed = rtrim($sizeString, ',');
	return $trimmed;
}
// function sanitize($dirty){
// 	return htmlentities($dirty);
// }
// require_once'../core/init.php';

// $sql = "SELECT * from brand ORDER BY brand";
// $results = $db->query($sql);
// $errors = array();


//if form submitted//

// if (empty($_POST["a_brand"])) {
// 	echo 0;

	// php method
	// $brand = sanitize($_POST['brand']);
	// check brand is blank
	// if ($_POST['brand'] =='') {
	// 	$errors[] .='You must enter a brand';
	// 	# code...
	// }
	// //check if brand exists in database
	// $sql1 = "SELECT * FROM brand WHERE brand = '$brand' ";
	// $results1 = $db->query($sql1);
	// $count = mysqli_num_rows($results1);
	// if($count > 0)
	// {
	// 	$errors[] .= 'That Band Allready Exists Please choose another brand';
	// }

	// //display errors
	// if (!empty($errors)) {
	// 	# code...
	// 	echo display_errors($errors);
	// }else{
	// 	//add brand to database
	// 	$sql2 = "INSERT INTO brand (brand) VALUES ('$brand') ";
	// 	$db->query($sql2);
	// 	header('Location: brand.php');
	// }
// }else if (isset($_POST['a_brand'])) {

// 	$brand = $_POST['a_brand'];
//  	$sql1 = "SELECT * FROM brand WHERE brand = '$brand' ";
//  	$results1 = $db->query($sql1);
//  	$count = mysqli_num_rows($results1);
//  	if($count > 0)
//  	{	
//  	 	echo 1;
//  	}else{
//  		$sql2 = "INSERT INTO brand (brand) VALUES ('$brand') ";
//  		$db->query($sql2);
//  		echo 2;
//  	}
// }else
// {
// 	echo 3;
// }

?>
