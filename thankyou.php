<?php 
require'core/db.php';


//use payment portal to get paid this is using any payement portal item paid set to 1 after submit form.


//get the rest post data
$full_name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$street = sanitize($_POST['street']);
$street2 = sanitize($_POST['street2']);
$city = sanitize($_POST['city']);
$state = sanitize($_POST['state']);
$zip_code = sanitize($_POST['zip_code']);
$country = sanitize($_POST['country']);
$tax = sanitize($_POST['tax']);
$sub_total = sanitize($_POST['sub_total']);
$grand_total = sanitize($_POST['grand_total']);
$cart_id = sanitize($_POST['cart_id']);
$description = sanitize($_POST['description']);
$charge_amount = number_format($grand_total,2) * 100;


//adjjust inventory
$itemQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
$iresult = mysqli_fetch_assoc($itemQ);
$items = json_decode($iresult['items'],true);
foreach ($items as $item) {
	$newSizes = array();
	$item_id = $item['id'];
	$productQ = $db->query("SELECT sizes FROM products WHERE id = '{$item_id}'");
	$product = mysqli_fetch_assoc($productQ);
	$sizes = sizesToArray($product['sizes']);

	foreach ($sizes as $size) {
			if ($size['size'] == $item['size']) {
				$q = $size['quantity'] - $item['quantity'];
				$newSizes[] = array('size' => $size['size'], 'quantity' => $q, 'threshold' => $size['threshold'] );
			}else{
				$newSizes[] = array('size' => $size['size'], 'quantity' => $size['quantity'] , 'threshold' => $size['threshold'] );
			}
		}
		$sizeString = sizesToString($newSizes);	
		$db->query("UPDATE products SET sizes = '{$sizeString}' WHERE id = '{$item_id}'");
}

//set paid to 1 when payment is maid
//update cart
$db->query("UPDATE cart SET paid = 1 WHERE id = '{$cart_id}'");
$db->query("INSERT INTO transactions 
	(cart_id,full_name,email,street,street2,city,state,zip_code,country,sub_total,tax,grand_total,description) VALUES
	('$cart_id','$full_name','$email','$street','$street2','$city','$state','zip_code','country','$sub_total','$tax','$grand_total','$description')
	");
$domain = ($_SERVER['HTTP_HOST'] != 'localhost')? '.'.$_SERVER['HTTP_HOST']:false;
setcookie(CART_COOKIE,'',1,"/",$domain.false );
?>

<!doctype html>
<html lang="en" >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<script src="js/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<title>ThankYou</title>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 upper-head">
			<div class="col-md-8 col-md-offset-2 wow slideInRight">
				<h1 class="text-center first">UniQueStore</h1>
				<h1 class="text-center second">Thank You</h1>
				<h3 class="text-center third"><strong>FOR SHOPPING WITH US</strong></h3>
			</div>
		</div>
		<div class="col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2 padding-tb-40 wow slideInLeft">
			<h3 class="text-center">Your payment has been successfully Paid. You have been emailed a receipt. Please check ypur spam folder if it is not in your inbox. Additionally You can print this page as a receipt.</h3>
		</div>
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
			    <thead>
			      <tr>
			        <th class="text-center">Receipt No</th>
			        <th class="text-center">Name</th>
			        <th class="text-center">Amount</th>
			        <th class="text-center">Address</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr class="text-center">
			        <td><strong><?=$cart_id;?></strong></td>
			        <td><?=$full_name;?></td>
			        <td><?=money($grand_total);?></td>
			        <td><?=$street;?> &nbsp <?=(($street2 != '')?$street2.'<br>':'');?> &nbsp <?=$city.', '.$state.', '.$zip_code;?> &nbsp <?=$country;?></td>
			      </tr>
			    </tbody>
			 </table>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 padding-tb-40">
			<div class="col-xs-12 col-sm-5 col-md-5 wow slideInLeft">
				<img src="img/slider/email.png" class="img-responsive">
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 wow slideInRight">
				<h2>Check your email for gift voucher</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, dolore, impedit eveniet necessitatibus voluptate distinctio quam repellendus voluptates voluptatum inventore rem sapiente minus esse saepe iste harum architecto numquam quis </p>
			</div>
		</div>
		<div class="col-xs-10 col-md-6 col-xs-offset-1 col-md-offset-3 padding-tb-40">
			<div class="text-center">
				<a href="index.php">Back to homepage</a>
			</div>
			<h3 class="text-center">Join us on social media</h3>
			<div id="social text-center wow pulse">
				<a class="facebookBtn smGlobalBtn" href="#" ></a>
				<a class="twitterBtn smGlobalBtn" href="#" ></a>
				<a class="googleplusBtn smGlobalBtn" href="#" ></a>
				<a class="linkedinBtn smGlobalBtn" href="#" ></a>
				<a class="pinterestBtn smGlobalBtn" href="#" ></a>
				<a class="tumblrBtn smGlobalBtn" href="#" ></a>
				<a class="rssBtn smGlobalBtn" href="#" ></a>
			</div>
	
		</div>
	</div>
</div>
</body>
</html>